import React from 'react';
import ReactDOM from 'react-dom';
import LeagueSidebar from '../app/components/events/league-sidebar/league-sidebar';

export default function() {
	ReactDOM.render(<LeagueSidebar/>, document.getElementById('leaguePortletRoot'));
}