import FastDealData from '../../../entities/ui/modals/fast-deal/fast-deal-data';
import CommonAction from '../../../actions/common/types/common-action';
import { UiActionType } from '../../../actions/ui/ui-action-type';

export default function fastDealReducer(state: FastDealData = new FastDealData(), action: CommonAction) {
    if (action.type === UiActionType.UPDATE_FAST_DEAL_DATA) {
        return new FastDealData(action.payload);
    }
    return state;
}