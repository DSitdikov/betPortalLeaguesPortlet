import { combineReducers } from 'redux';
import fastDealReducer from './fast-deal/fast-deal.readucer';
import uiCommonReducer from './ui-common/ui-common.reducer';
import modalReducer from './modal/modal.reducer';
import leaguesReducer from './leagues/leagues.reducer';
import eventsReducer from './events/events.reducer';
import sportReducer from './sport/sport.reducer';
import marketsReducer from './markets/markets.reducer';
import periodsReducer from './periods/periods.reducer';
import authReducer from '../entities/auth/auth.reducer';

const uiReducer = combineReducers({
    common: uiCommonReducer,
    modal: modalReducer,
    fastDealData: fastDealReducer,
    sport: sportReducer,
    leagues: leaguesReducer,
    events: eventsReducer,
    periods: periodsReducer,
    markets: marketsReducer,
    authentication: authReducer
});

export default uiReducer;