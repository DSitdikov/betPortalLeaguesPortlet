import authReducer from './auth.reducer';
import AuthUiState from '../../../entities/ui/auth-state/auth-ui.state';
import AppError from '../../../exceptions/app-error';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';

describe('AuthReducer', () => {
    const reducer = authReducer;
    let state;

    beforeEach(() => {
        state = new AuthUiState();
    });

    it('should reset auth ui state if action `logout` is performed', () => {
        state.error = new AppError('some error');

        const resultState = reducer(
            state,
            {
                type: DomainModelActionType.LOGOUT_SUCCESS,
                payload: undefined
            }
        );

        expect(resultState.busy).toBeFalsy();
        expect(resultState.error).toBeFalsy();
        expect(resultState.logged).toBeFalsy();
    });
});