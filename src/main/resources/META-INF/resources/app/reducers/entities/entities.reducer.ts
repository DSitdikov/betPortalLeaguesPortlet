import { combineReducers } from 'redux';
import sportReducer from './sport/sport.reducer';
import leaguesReducer from './leagues/leagues.reducer';
import eventsReducer from './events/events.reducer';
import periodsReducer from './periods/periods.reducer';
import marketsReducer from './markets/markets.reducer';
import ticketsReducer from './tickets/tickets.reducer';
import customerReducer from './customer/customer.reducer';
import authReducer from './auth/auth.reducer';

const entitiesReducer = combineReducers({
    sport: sportReducer,
    leagues: leaguesReducer,
    events: eventsReducer,
    periods: periodsReducer,
    markets: marketsReducer,
    tickets: ticketsReducer,
    customer: customerReducer,
    authentication: authReducer
});

export default entitiesReducer;