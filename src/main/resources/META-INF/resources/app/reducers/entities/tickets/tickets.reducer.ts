import CommonAction from '../../../actions/common/types/common-action';
import Ticket from '../../../entities/domain-model/ticket/ticket';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';

const initialState = new Map<number, Ticket>();

export default function ticketsReducer(state: Map<number, Ticket> = initialState, action: CommonAction) {
    switch (action.type) {
        case DomainModelActionType.FETCH_TICKETS_SUCCESS:
            return action.payload as Map<number, Ticket>;
        case DomainModelActionType.ADD_TICKET_SUCCESS:
            const ticket = action.payload as Ticket;
            return new Map<number, Ticket>([
                [ticket.id, ticket],
                ...Array.from(state.entries())
            ]);
        default:
            return state;
    }
}