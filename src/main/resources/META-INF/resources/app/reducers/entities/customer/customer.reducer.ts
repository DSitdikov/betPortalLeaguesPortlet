import CommonAction from '../../../actions/common/types/common-action';
import Customer from '../../../entities/domain-model/customer/customer';
import { DomainModelActionType } from '../../../actions/domain-model/domain-model-action-type';

export default function customerReducer(state: Customer = new Customer(), action: CommonAction) {
    switch (action.type) {
        case DomainModelActionType.LOGIN_SUCCESS: {
            return new Customer(action.payload);
        }
        case DomainModelActionType.LOGOUT_SUCCESS: {
            return new Customer();
        }
        default:
            return state;
    }
}
