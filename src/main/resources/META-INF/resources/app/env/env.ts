import { EnvType } from '../enums/env-type';

const env = {
    sportId: 1032,
    refreshInterval: 60 * 1000,
    requestDelay: 600,
    closeTimeout: 3000,
    mockHttpStatusHandler: true,
    mockRequestMethod: true,
    pushApiUrl: 'ws://localhost:9091',
    socketReconnectInterval: 0,
    apiBaseUrl: '/data',
    userCookieName: 'user',
    apiUrls: {
        league: {
            base: '',
            list: '/real.leaguesEvents.3.json?sportId=:sportId'
        },
        bet: {
            base: '',
            list: '/real.bet.fastbet.all.2.json',
            add: '/bet.fastbet.add.json'
        },
        auth: {
            base: '',
            login: '/login.success.json',
            logout: '/logout.success.json'
        },
        user: {
            base: '',
            info: '/info.success.json'
        }
    }
};

if (process.env.NODE_ENV === EnvType.PROD) {
    Object.assign(env, {
        refreshInterval: 3 * 1000,
        pushApiUrl: `ws://${location.host}/ws`,
        socketReconnectInterval: 0,
        apiBaseUrl: '/api',
        apiUrls: {
            league: {
                base: '/price',
                list: '/leaguesEvents'
            },
            bet: {
                base: '/bet',
                list: '/fastbet/all',
                add: '/fastbet'
            },
            auth: {
                base: '/auth',
                login: '/login',
                logout: '/logout',
            },
            user: {
                base: '/user',
                info: '/info'
            }
        },
        mockRequestMethod: false,
        mockHttpStatusHandler: false,
    });
}

export default env;
