import { Action, ActionCreator, Dispatch } from 'redux';
import Bet from '../../../entities/domain-model/bet/bet';
import { DomainModelActionType } from '../domain-model-action-type';
import StoreState from '../../../entities/store-state';
import { showErrorNotification } from '../../ui/common/common.actions';
import { ThunkAction } from 'redux-thunk';
import { closeModal, showModal } from '../../ui/modal/modal.actions';
import BetDataLoader from '../../../services/data-loaders/bet-data-loader/bet-data-loader';
import FastDealData from '../../../entities/ui/modals/fast-deal/fast-deal-data';
import CommonAction from '../../common/types/common-action';
import { ModalType } from '../../../enums/modal-type';
import { updateFastDealData } from '../../';
import Ticket from '../../../entities/domain-model/ticket/ticket';
import Market from '../../../entities/domain-model/markets/common/market';
import LeagueEventPeriod from '../../../entities/domain-model/league-event-period/league-event-period';
import LeagueEvent from '../../../entities/domain-model/league-event/league-event';

export const fetchTicketsSuccess: ActionCreator<CommonAction> = (bets: Bet[]) => ({
    type: DomainModelActionType.FETCH_TICKETS_SUCCESS,
    payload: bets
});

export const doBet: ActionCreator<ThunkAction<CommonAction, StoreState, void>> = (data: FastDealData) => {
    return (dispatch: Dispatch<StoreState>): CommonAction => {
        dispatch(showModal(ModalType.BET_CONFIRM));
        return dispatch(updateFastDealData(data));
    };
};

export const addTicket: ActionCreator<ThunkAction<Promise<Action>, StoreState, void>> = (data: FastDealData) => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): Promise<Action> => {
        dispatch(closeModal());

        const loader = new BetDataLoader();
        const customerId = getState().entities.customer.id;
        return loader.addBet(customerId, data)
            .then(bet => {
                const entities = getState().entities;

                const market = entities.markets.get(bet.marketId) as Market;
                const period = entities.periods.get(market.periodId) as LeagueEventPeriod;
                const event = entities.events.get(period.eventId) as LeagueEvent;
                const ticket = Ticket.createTicketFromBet(bet, market, event);

                return dispatch(addTicketSuccess(ticket));
            })
            .catch(error => dispatch(showErrorNotification(error)));
    };
};

const addTicketSuccess: ActionCreator<CommonAction> = (ticket: Ticket) => ({
    type: DomainModelActionType.ADD_TICKET_SUCCESS,
    payload: ticket
});