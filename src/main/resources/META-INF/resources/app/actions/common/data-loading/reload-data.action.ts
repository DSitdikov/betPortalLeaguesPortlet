import { ThunkAction } from 'redux-thunk';
import { Action, ActionCreator, Dispatch } from 'redux';
import { fetchTicketsSuccess, fetchLeaguesSuccess } from '../../';
import StoreState from '../../../entities/store-state';
import { doNothing } from '../do-nothing/do-nothing.action';
import InitialDataLoader from '../../../services/data-loaders/initial-data-loader/initial-data-loader';

export const reloadData: ActionCreator<ThunkAction<Promise<Action>, StoreState, void>> = () => {
    return (dispatch: Dispatch<StoreState>, getState: () => StoreState): Promise<Action> => {
        const dataLoader = new InitialDataLoader();
        const customerId = getState().entities.customer.id;
        const sportId = getState().entities.sport.id;
        return dataLoader.loadData(customerId, sportId)
            .then(results => {
                const [leagueEntities, tickets] = results;
                dispatch(fetchLeaguesSuccess(leagueEntities));
                dispatch(fetchTicketsSuccess(tickets));
            })
            .catch(() => dispatch(doNothing()))
            .then(() => dispatch(doNothing()));
    };
};