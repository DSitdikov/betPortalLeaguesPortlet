import { Action, ActionCreator } from 'redux';
import StoreState from '../../../../entities/store-state';
import { Dispatch } from 'react-redux';
import { ThunkAction } from 'redux-thunk';
import AuthData from '../../../../entities/domain-model/auth/auth-data';
import AppError from '../../../../exceptions/app-error';
import CommonAction from '../../types/common-action';
import Customer from '../../../../entities/domain-model/customer/customer';
import { DomainModelActionType } from '../../../domain-model/domain-model-action-type';
import { setBusy, showErrorNotification } from '../../../ui/common/common.actions';
import AuthenticationDataLoader
    from '../../../../services/data-loaders/authentication-data-loader/authentication-data-loader';
import UserDataLoader from '../../../../services/data-loaders/user/user-data-loader';
import { doNothing } from '../../do-nothing/do-nothing.action';

export const initCustomer: ActionCreator<ThunkAction<Promise<Action>, StoreState, null>> = () => {
    return (dispatch: Dispatch<StoreState>): Promise<Action> => {
        const customer = UserDataLoader.getInfoFromStorage();

        if (!customer) {
            return Promise.resolve(dispatch(doNothing()));
        }

        return Promise.resolve(dispatch(loginSuccess(customer)));
    };
};
export const login: ActionCreator<ThunkAction<Promise<Action>, StoreState, null>> = (data: AuthData) => {
    return (dispatch: Dispatch<StoreState>): Promise<Action> => {
        // TODO: Make common actions
        dispatch(loginRequest());

        const authLoader = new AuthenticationDataLoader();
        return authLoader.login(data)
            .then(customer => {
                return dispatch(loginSuccess(customer));
            })
            .catch(error => dispatch(loginFail(error)));
    };
};

export const logout: ActionCreator<ThunkAction<Promise<Action>, StoreState, null>> = () => {
    return (dispatch: Dispatch<StoreState>): Promise<Action> => {
        dispatch(setBusy(true));

        const authLoader = new AuthenticationDataLoader();
        return authLoader.logout()
            .then(() => dispatch(logoutSuccess()))
            .catch(error => dispatch(showErrorNotification(error)))
            .then(() => dispatch(setBusy(false)));
    };
};

export const loginRequest: ActionCreator<Action> = () => ({ type: DomainModelActionType.LOGIN_REQUEST });
export const loginSuccess: ActionCreator<Action> = (customer: Customer) => ({
    type: DomainModelActionType.LOGIN_SUCCESS,
    payload: customer
});
export const loginFail: ActionCreator<CommonAction> = (error: AppError) => ({
    type: DomainModelActionType.LOGIN_FAILURE,
    payload: error
});

export const logoutSuccess: ActionCreator<Action> = () => ({ type: DomainModelActionType.LOGOUT_SUCCESS });
