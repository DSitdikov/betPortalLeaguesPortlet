import AuthenticationDataLoader
    from '../../../../services/data-loaders/authentication-data-loader/authentication-data-loader';
import Mock = jest.Mock;
import Customer from '../../../../entities/domain-model/customer/customer';
import { login } from './auth-data.action';
import StoreState from '../../../../entities/store-state';
import { DomainModelActionType } from '../../../domain-model/domain-model-action-type';

describe('AuthDataActions', () => {
    AuthenticationDataLoader.prototype.login = jest.fn();
    AuthenticationDataLoader.prototype.logout = jest.fn();

    const mockLogin = AuthenticationDataLoader.prototype.login as Mock;
    const mockLogout = AuthenticationDataLoader.prototype.logout as Mock;
    const mockDispatch = jest.fn();

    beforeEach(() => {
        mockLogin.mockReturnValue(Promise.resolve(new Customer({ id: 'F901', login: 'F901' })));
        mockLogout.mockReturnValue(Promise.resolve(undefined));
    });

    describe('login action', () => {
        const AUTH_DATA = { login: 'F901', password: 'securityvipbet' };

        it('should send login request and appropriate action', () => {
            login(AUTH_DATA)(mockDispatch, () => ({} as StoreState), null);

            const action = mockDispatch.mock.calls[0][0];
            expect(action.type).toEqual(DomainModelActionType.LOGIN_REQUEST);
        });
    });

    describe('logout action', () => {

    });
});