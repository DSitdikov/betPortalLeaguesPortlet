export const AppNotificationType = {
    SUCCESS: 'success',
    INFO: 'info',
    DANGER: 'danger'
};