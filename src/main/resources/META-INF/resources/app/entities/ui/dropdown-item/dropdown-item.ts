export default class DropdownItem {
    id: number;
    title: string;
    value: number;
    selected: boolean;

    constructor(data?: any) {
        data = data || {};

        this.id = data.id || 0;
        this.title = data.title || '';
        this.value = data.value || 0;
        this.selected = Boolean(data.selected);
    }
}
