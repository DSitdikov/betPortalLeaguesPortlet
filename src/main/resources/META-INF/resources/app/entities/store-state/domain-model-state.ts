import LeagueEventPeriod from '../domain-model/league-event-period/league-event-period';
import Sport from '../domain-model/sport/sport';
import League from '../domain-model/league/league';
import LeagueEvent from '../domain-model/league-event/league-event';
import Market from '../domain-model/markets/common/market';
import Ticket from '../domain-model/ticket/ticket';
import Customer from '../domain-model/customer/customer';

export default interface DomainModelState {
    sport: Sport;
    leagues: Map<number, League>;
    events: Map<number, LeagueEvent>;
    periods: Map<number, LeagueEventPeriod>;
    markets: Map<number, Market>;
    tickets: Map<number, Ticket>;
    customer: Customer;
    authentication: {
        logged?: boolean;
    };
}