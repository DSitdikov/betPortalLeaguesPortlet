export default class Sport {
    id: number;
    name: string;
    expanded: boolean;

    constructor(data?: any) {
        data = data || {};

        this.id = data.id || 0;
        this.name = data.name || '';
        this.expanded = data.expanded !== undefined ? Boolean(data.expanded) : true;
    }
}