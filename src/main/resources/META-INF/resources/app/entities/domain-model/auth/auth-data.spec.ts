import AuthData from './auth-data';

describe('AuthData', () => {
    it('should create default instance without constructor parameters', () => {
        const authData = new AuthData();

        expect(authData.login).toEqual('');
        expect(authData.password).toEqual('');
        expect(authData.rememberMe).toEqual(false);
    });

    it('should parse raw data correctly', () => {
        const authData = new AuthData({
            login: 'F901',
            password: 'securityvipbet',
            rememberMe: true
        });

        expect(authData.login).toEqual('F901');
        expect(authData.password).toEqual('securityvipbet');
        expect(authData.rememberMe).toEqual(true);
    });
});