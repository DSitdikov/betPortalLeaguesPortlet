import SimpleLineMarket from '../simple-line-market/simple-line-market';
import { OutcomeType } from '../../outcome/outcome-type';
import LeagueEvent from '../../league-event/league-event';
import { MarketType } from '../common/market-type';

export default class DoubleChanceMarket extends SimpleLineMarket {

    constructor(data: any = {}) {
        super({
            ...data,
            type: MarketType.DOUBLE_CHANCE
        });
    }

    protected selectOutcomeName(outcomeType: string, event: LeagueEvent): string {
        let name = '';
        switch (outcomeType) {
            case OutcomeType.HOME:
                name = `2X (${event.home} ML or Draw)`;
                break;
            case OutcomeType.DRAW:
                name = `12 (${event.home} ML or ${event.away} ML)`;
                break;
            case OutcomeType.AWAY:
                name = `1X (${event.away} ML or Draw)`;
                break;
            default:
                break;
        }

        return name;
    }
}
