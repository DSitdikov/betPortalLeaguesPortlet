import SimpleLineMarket from '../simple-line-market/simple-line-market';

/**
 * Used as the stub for testing
 */
export default class SimpleLineMarketStub extends SimpleLineMarket {

    constructor(data: any = {}) {
        super(data);
    }

    protected selectOutcomeName() {
        return '';
    }
}
