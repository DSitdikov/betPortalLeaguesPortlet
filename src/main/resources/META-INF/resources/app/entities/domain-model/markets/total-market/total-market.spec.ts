import TotalMarket from './total-market';
import { OutcomeType } from '../../outcome/outcome-type';

describe('TotalMarket', () => {
    it('should create default instance if there are no constructor parameters', () => {
        const market = new TotalMarket();

        expect(market.id).toEqual(0);
        expect(market.originId).toEqual(0);
        expect(market.periodId).toEqual(0);
        expect(market.type).toEqual('Totals');
        expect(market.points).toEqual(0);
        expect(market.outcomeB.hasValue).toEqual(false);
        expect(market.outcomeA.hasValue).toEqual(false);
    });

    it('should parse raw total', () => {
        const market = new TotalMarket({
            id: 47371,
            periodId: 47878,
            points: 2.50,
            over: 2.11,
            under: 1.80
        });

        expect(market.id).toEqual(47371);
        expect(market.originId).toEqual(47371);
        expect(market.periodId).toEqual(47878);
        expect(market.type).toEqual('Totals');
        expect(market.points).toEqual(2.50);
        expect(market.outcomeA.value).toEqual(2.11);
        expect(market.outcomeA.tip).toEqual('Over 2.50');
        expect(market.outcomeB.value).toEqual(1.80);
        expect(market.outcomeB.tip).toEqual('Under 2.50');
    });

    it('should override origin id', () => {
        const market = new TotalMarket({
            id: 1,
            originId: 2
        });

        expect(market.id).toEqual(1);
        expect(market.originId).toEqual(2);
    });

    it('should correct copy total', () => {
        const market = new TotalMarket({
            id: 47371,
            periodId: 47878,
            points: 2.50,
            over: 2.11,
            under: 1.80
        });
        const marketCopy = new TotalMarket(market);

        expect(marketCopy.id).toEqual(47371);
        expect(market.originId).toEqual(47371);
        expect(market.periodId).toEqual(47878);
        expect(market.type).toEqual('Totals');
        expect(marketCopy.points).toEqual(2.50);
        expect(marketCopy.outcomeA.value).toEqual(2.11);
        expect(marketCopy.outcomeA.tip).toEqual('Over 2.50');
        expect(marketCopy.outcomeB.value).toEqual(1.80);
        expect(marketCopy.outcomeB.tip).toEqual('Under 2.50');
    });

    it('should return default outcome name if outcome type is undefined or wrong', () => {
        const market = new TotalMarket();

        expect(market.getOutcomeName('random value')).toEqual('');
        expect(market.getOutcomeName('')).toEqual('');
    });

    it('should return outcome name', () => {
        const market = new TotalMarket({
            id: 47371,
            periodId: 47878,
            points: 2.50,
            over: 2.11,
            under: 1.80
        });

        expect(market.getOutcomeName(OutcomeType.OVER)).toEqual('Over 2.50');
        expect(market.getOutcomeName(OutcomeType.UNDER)).toEqual('Under 2.50');
    });
});