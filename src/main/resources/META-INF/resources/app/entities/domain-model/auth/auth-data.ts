export default class AuthData {
    login: string;
    password: string;
    rememberMe: boolean;

    constructor(data?: any) {
        data = data || {};

        this.login = data.login || '';
        this.password = data.password || '';
        this.rememberMe = Boolean(data.rememberMe);
    }
}
