export default class PushNotification {
    actionType: string;
    entityType: string;
    data: any;

    constructor(data?: any) {
        data = data || {};

        this.actionType = data.actionType || '';
        this.entityType = data.entityType || '';
        this.data = data.data || {};
    }
}