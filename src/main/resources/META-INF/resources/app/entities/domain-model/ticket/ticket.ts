import Bet from '../bet/bet';
import Price from '../price/price';
import Market from '../markets/common/market';
import NumberFormatUtils from '../../../utils/number-format-utils';
import Outcome from '../outcome/outcome';
import LeagueEvent from '../league-event/league-event';

export default class Ticket {
    id: number;
    eventName: string;
    outcomeName: string;
    priceFormatted: string;
    amountFormatted: string;

    static createTicketFromBet(bet: Bet, market: Market, event: LeagueEvent): Ticket {
        const price = new Price(bet.price);
        const amount = NumberFormatUtils.isolateThousands(bet.amount);
        const outcome = new Outcome({
            event,
            market,
            outcomeType: bet.type
        });

        return new Ticket({
            id: bet.id,
            eventName: event.name,
            outcomeName: outcome.name,
            priceFormatted: price.formatted,
            amountFormatted: amount
        });
    }

    constructor(data?: any) {
        data = data || {};

        this.id = data.id || 0;
        this.eventName = data.eventName || '';
        this.outcomeName = data.outcomeName || '';
        this.priceFormatted = data.priceFormatted || '';
        this.amountFormatted = data.amountFormatted || '';
    }
}
