export default class Customer {
    id: string;
    login: string;

    constructor(data: any = {}) {
        this.id = data.id || '';
        this.login = data.login || '';
    }
}
