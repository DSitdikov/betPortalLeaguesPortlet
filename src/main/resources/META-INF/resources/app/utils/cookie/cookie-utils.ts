export default class CookieUtils {
    static getCookie(name: string) {
        const preparedName = name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1');
        const matches = document.cookie.match(new RegExp(`(?:^|; )${preparedName}=([^;]*)`));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }
}