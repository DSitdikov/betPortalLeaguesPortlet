export default class PushListener<T> {
    actionType: string;
    entityType: string;
    action: (data: any) => void;

    constructor(data?: any) {
        data = data || {};

        this.actionType = data.actionType || '';
        this.entityType = data.entityType || '';
        this.action = data.action || null;
    }

    invokeAction(data: T) {
        if (this.action) {
            this.action(data);
        }
    }
}