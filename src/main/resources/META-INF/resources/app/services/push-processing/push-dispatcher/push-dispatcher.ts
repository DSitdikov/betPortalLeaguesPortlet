import env from '../../../env/env';
import PushListener from '../push-listener/push-listener';
import PushNotification from '../../../entities/domain-model/push-notification/push-notification';
import Entity from '../../../entities/domain-model/entity/entity';

export default class PushDispatcher {
    listeners: PushListener<Entity>[];

    private logger: { log: (...args: any[]) => void };
    private socket: WebSocket;

    constructor(data?: any) {
        data = data || {};

        // noinspection TsLint
        this.logger = {
            // TODO: use logger
            log: (...args) => console.log(...args)
        };

        this.listeners = data.listeners || [];
    }

    connect() {
        this.socket = new WebSocket(env.pushApiUrl);
        this.socket.onopen = this.handleConnectionOpen.bind(this);
        this.socket.onclose = this.handleConnectionClose.bind(this);
        this.socket.onerror = this.handleError.bind(this);
        this.socket.onmessage = this.handleMessage.bind(this);
    }

    private handleConnectionOpen() {
        this.logger.log('Connection has been established.');
    }

    private handleConnectionClose(event: CloseEvent) {
        if (event.wasClean) {
            this.logger.log('Connection has been closed.');
        } else {
            this.logger.log('Connection has been removed.');
            // Try to reconnect to web socket
            // if reconnect interval is specified
            if (env.socketReconnectInterval) {
                setTimeout(() => this.connect(), env.socketReconnectInterval);
            }
        }
        this.logger.log('Code:', event.code);
        this.logger.log('Reason:', event.reason);
    }

    private handleError(error: Event) {
        this.logger.log('Error:', error);
    }

    private handleMessage(event: MessageEvent) {
        this.logger.log('Received data:' + event.data);

        const notification = new PushNotification(JSON.parse(event.data));
        this.logger.log('Parsed message:', notification);

        // Notify listeners with specific action/entity type
        this.listeners.forEach(listener => {
            if (listener.actionType === notification.actionType
                && listener.entityType === notification.entityType) {
                this.logger.log('invoke action with data:', notification.data);
                listener.invokeAction(notification.data);
            }
        });
    }
}