import PushListener from './push-listener';

describe('PushListener', () => {
    it('should create default listener', () => {
        const listener = new PushListener();

        expect(listener.entityType).toEqual('');
        expect(listener.actionType).toEqual('');
        expect(listener.action).toBeNull();
    });
});