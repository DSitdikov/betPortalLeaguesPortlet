import PushDispatcher from './push-dispatcher';

describe('PushDispatcher', () => {
    it('should create default dispatcher without listeners', () => {
        const dispatcher = new PushDispatcher();

        expect(dispatcher.listeners).toEqual([]);
    });
});