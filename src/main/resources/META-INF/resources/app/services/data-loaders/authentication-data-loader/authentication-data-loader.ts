import BasicDataLoader from '../basic-data-loader/basic-data-loader';
import env from '../../../env/env';
import AuthData from '../../../entities/domain-model/auth/auth-data';
import DataLoader from '../common/data-loader';
import Customer from '../../../entities/domain-model/customer/customer';

export default class AuthenticationDataLoader {
    loader: DataLoader;

    constructor(data: any = {}) {
        this.loader = data.loader || new BasicDataLoader({ baseApiUrlSuffix: env.apiUrls.auth.base });
    }

    login(data: AuthData): Promise<Customer> {
        return this.loader.post(env.apiUrls.auth.login, data)
            .then(rawCustomer => {
                return new Customer(rawCustomer);
            });
    }

    logout(): Promise<void> {
        return this.loader.post(env.apiUrls.auth.logout);
    }
}
