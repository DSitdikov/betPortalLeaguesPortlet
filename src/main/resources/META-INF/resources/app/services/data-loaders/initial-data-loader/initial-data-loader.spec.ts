import InitialDataLoader from './initial-data-loader';
import env from '../../../env/env';
import Mock = jest.Mock;
import League from '../../../entities/domain-model/league/league';
import Bet from '../../../entities/domain-model/bet/bet';
import LeagueEventPeriod from '../../../entities/domain-model/league-event-period/league-event-period';
import LeagueEvent from '../../../entities/domain-model/league-event/league-event';
import Ticket from '../../../entities/domain-model/ticket/ticket';
import TotalMarket from '../../../entities/domain-model/markets/total-market/total-market';

describe('InitialDataLoader', () => {
    let service;
    const betLoader = {
        getBetList: jest.fn()
    };
    const leagueLoader = {
        getLeagueEntities: jest.fn()
    };

    const SPORT_ID = env.sportId;

    beforeEach(() => {
        (<Mock> betLoader.getBetList).mockReturnValue(Promise.resolve([
            new Bet({ id: 1, marketId: 101, type: 'under' }),
            new Bet({ id: 2, marketId: 102, type: 'over' })
        ]));
        (<Mock> leagueLoader.getLeagueEntities).mockReturnValue(Promise.resolve({
            leagues: new Map([[11, new League({ id: 11 })]]),
            events: new Map([[800, new LeagueEvent({ id: 800 })]]),
            periods: new Map([[200, new LeagueEventPeriod({ id: 200, eventId: 800 })]]),
            markets: new Map([
                [101, new TotalMarket({ id: 101, periodId: 200, over: 20, home: 10, points: 4 })],
                [102, new TotalMarket({ id: 102, periodId: 200, over: 20, home: 10, points: 5 })],
                [103, new TotalMarket({ id: 103, periodId: 200, over: 20, home: 10, points: 6 })]
            ])
        }));

        service = new InitialDataLoader({ leagueLoader, betLoader });
    });

    describe('#loadData', () => {
        it('should prepare tickets from bets and league entities', () => {
            return service.loadData(SPORT_ID)
                .then(results => {
                    const [leagueEntities, tickets] = results;

                    const leagues = leagueEntities.leagues;
                    expect(leagues.size).toEqual(1);

                    expect(tickets.size).toEqual(2);
                    expect((tickets.get(1) as Ticket).id).toEqual(1);
                    expect((tickets.get(1) as Ticket).outcomeName).toEqual('Under 4.00');
                    expect((tickets.get(2) as Ticket).outcomeName).toEqual('Over 5.00');
                });
        });
    });
});