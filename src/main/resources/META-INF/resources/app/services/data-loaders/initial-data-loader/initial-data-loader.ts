import BetDataLoader from '../bet-data-loader/bet-data-loader';
import LeagueDataLoader from '../league-data-loader/league-data-loader';
import Market from '../../../entities/domain-model/markets/common/market';
import LeagueEvent from '../../../entities/domain-model/league-event/league-event';
import Ticket from '../../../entities/domain-model/ticket/ticket';
import LeagueEventPeriod from '../../../entities/domain-model/league-event-period/league-event-period';

export default class InitialDataLoader {
    private leagueLoader: LeagueDataLoader;
    private betLoader: BetDataLoader;

    constructor(data: any = {}) {
        this.leagueLoader = data.leagueLoader || new LeagueDataLoader();
        this.betLoader = data.betLoader || new BetDataLoader();
    }

    loadData(customerId: string, sportId: number): Promise<any[]> {
        return Promise.all([
            this.leagueLoader.getLeagueEntities(sportId),
            this.betLoader.getBetList(customerId)
        ])
            .then(results => {
                const [leagueEntities, bets] = results;

                // ATTENTION!!! Now we should restore missing information about ticket
                // from loaded leagues
                // We try to find market and get information about league and event
                // that contains it
                const events = leagueEntities.events;
                const periods = leagueEntities.periods;
                const markets = leagueEntities.markets;

                const betsWithLoadedMarkets = bets.filter(bet => markets.has(bet.marketId));
                const tickets = betsWithLoadedMarkets.map(bet => {
                    const market = markets.get(bet.marketId) as Market;
                    const period = periods.get(market.periodId) as LeagueEventPeriod;
                    const event = events.get(period.eventId) as LeagueEvent;

                    return [bet.id, Ticket.createTicketFromBet(bet, market, event)];
                }) as [[number, Ticket]];
                const ticketMap = new Map<number, Ticket>(tickets);
                return [leagueEntities, ticketMap];
            });
    }
}