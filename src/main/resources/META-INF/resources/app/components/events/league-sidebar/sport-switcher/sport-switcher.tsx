import * as React from 'react';
import Sport from '../../../../entities/domain-model/sport/sport';
import classNames from 'classnames';
import { Action } from 'redux';
import { connect, Dispatch } from 'react-redux';
import StoreState from '../../../../entities/store-state';
import CommonAction from '../../../../actions/common/types/common-action';
import { toggleExpandStateOfSport } from '../../../../actions/ui/sport-switcher/sport-switcher.actions';
import SportUiState from '../../../../entities/ui/sport-ui-state/sport-ui-state';

interface DispathToPropsType {
    onToggleExpandStateOfSport: (id: number) => Action;
}

interface StateToPropsType {
    sport: Sport;
    sportUiState: SportUiState;
}

type Props = DispathToPropsType & StateToPropsType;

class SportSwitcher extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        const { sport, sportUiState, onToggleExpandStateOfSport } = this.props;

        const btnExpandStyleClass = classNames({
            'a-sport-switcher__btn-expand': true,
            'a-sport-switcher__btn-expand_active': sportUiState.expanded
        });

        return (
            <div className="a-sport-switcher">
                <div className="a-sport-switcher__items">
                    <div className="a-sport-switcher__item"/>
                    <div className="a-sport-switcher__item"/>
                    <div className="a-sport-switcher__item"/>
                    <div className="a-sport-switcher__item"/>
                    <div className="a-sport-switcher__item"/>
                    <div className="a-sport-switcher__item"/>
                </div>
                <div className="a-sport-switcher__selected-item">
                    <div className="a-sport-switcher__selected-item-title">{sport.name}</div>
                    <div className={btnExpandStyleClass} onClick={() => onToggleExpandStateOfSport(sport.id)}>
                        <div className="a-vip-icon a-vip-icon_arrow-dropdown"/>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        sport: state.entities.sport,
        sportUiState: state.ui.sport
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onToggleExpandStateOfSport: (id: number) => dispatch(toggleExpandStateOfSport(id))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SportSwitcher);
