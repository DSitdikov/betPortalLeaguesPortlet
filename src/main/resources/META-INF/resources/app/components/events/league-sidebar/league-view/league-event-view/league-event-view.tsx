import * as React from 'react';
import { Action } from 'redux';
import { connect, Dispatch } from 'react-redux';
import classNames from 'classnames';
import CommonAction from '../../../../../actions/common/types/common-action';
import { toggleSelectionOfEvent } from '../../../../../actions';
import LeagueEvent from '../../../../../entities/domain-model/league-event/league-event';
import StoreState from '../../../../../entities/store-state';
import EventUiState from '../../../../../entities/ui/event-ui-state/event-ui-state';

interface StateToPropsType {
    events: Map<number, LeagueEvent>;
    eventUiState: Map<number, EventUiState>;
}

interface DispathToPropsType {
    onToggleSelectionOfEvent: (id: number) => Action;
}

interface Props extends DispathToPropsType, StateToPropsType {
    eventId: number;
}

class LeagueEventView extends React.Component<Props> {

    constructor(props: Props) {
        super(props);
    }

    render() {
        const { onToggleSelectionOfEvent } = this.props;
        const eventId = this.props.eventId;
        const event = this.props.events.get(eventId) || new LeagueEvent();
        const eventUiState = this.props.eventUiState.get(eventId) || new EventUiState();

        const checkboxStyleClass = classNames({
            'a-checkbox': true,
            'a-checkbox_selected': eventUiState.selected
        });

        return (
            <li className="a-league-event" onClick={() => onToggleSelectionOfEvent(event.id)}>
                <div className="a-league-event__checkbox">
                    <div className={checkboxStyleClass}>
                        <div className="a-vip-icon a-vip-icon_checkmark"/>
                    </div>
                </div>
                <div className="a-league-event__title">
                    <div className="a-league-event__team">{event.home}</div>
                    <div className="a-league-event__team">{event.away}</div>
                </div>
                <div className="a-league-event__start"/>
                <div className="a-league-event__btn-favorite"/>
            </li>
        );
    }
}

function mapStateToProps(state: StoreState): StateToPropsType {
    return {
        eventUiState: state.ui.events,
        events: state.entities.events
    };
}

function mapDispatchToProps(dispatch: Dispatch<CommonAction>): DispathToPropsType {
    return {
        onToggleSelectionOfEvent: (id: number) => dispatch<any>(toggleSelectionOfEvent(id))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LeagueEventView);