import * as React from 'react';
import * as ReactDOM from 'react-dom';
import LeagueSidebar from './app/components/events/league-sidebar/league-sidebar';
import registerServiceWorker from './register-service-worker';
import './styles.scss';
import thunk from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from './app/reducers/index';
import { Provider } from 'react-redux';
import StoreState from './app/entities/store-state';
import { updateMarket } from './app/actions';
import PushDispatcher from './app/services/push-processing/push-dispatcher/push-dispatcher';
import PushListener from './app/services/push-processing/push-listener/push-listener';
import { PushActionType } from './app/services/push-processing/common/push-action-type';
import { PushEntityType } from './app/services/push-processing/common/push-entity-type';
import Market from './app/entities/domain-model/markets/common/market';
import { composeWithDevTools } from 'redux-devtools-extension';

// import * as createLogger from 'redux-logger';

const middleware = [
    thunk
];

// if (process.env.NODE_ENV !== 'production') {
//     middleware.push((createLogger as any)());
// }

const store = createStore<StoreState>(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middleware))
);

// Create dispatcher and listeners of push notifications (web socket)
const pushDispatcher = new PushDispatcher({
    listeners: [
        new PushListener<Market>({
            actionType: PushActionType.UPDATE,
            entityType: PushEntityType.MARKET,
            action: (market: Market) => store.dispatch(updateMarket(market))
        })
    ]
});
pushDispatcher.connect();

ReactDOM.render(
    <Provider store={store}>
        <LeagueSidebar/>
    </Provider>,
    document.getElementById('leaguePortletRoot') as HTMLElement
);
registerServiceWorker();